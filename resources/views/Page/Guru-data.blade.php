<!DOCTYPE html>
<!--
This is a starter Layout page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('Layout.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('Layout.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('Layout.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Data Guru</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Data Guru</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="card card-info card-outline">
                    <div class="card-header">
                        <div class="card-tools">
                            <a href="{{route('Guru-create')}}" class="btn btn-success">Tambah Data <i class="fas fa-plus-square"></i></a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>id</th>
                                <th>user_id</th>
                                <th>nip</th>
                                <th>nama</th>
                                <th>tempat_lahir</th>
                                <th>tgl_lahir</th>
                                <th>gender</th>
                                <th>phone_number</th>
                                <th>email</th>
                                <th>alamat</th>
                                <th>pendidikan</th>
                            </tr>
                            @foreach ($Guru as $item )
                            <tr>
                                <td>{{ $item->id}}</td>
                                <td>{{ $item->user_id}}</td>
                                <td>{{ $item->nip}}</td>
                                <td>{{ $item->nama}}</td>
                                <td>{{ $item->tempat_lahir}}</td>
                                <td>{{ $item->tgl_lahir}}</td>
                                <td>{{ $item->gender}}</td>
                                <td>{{ $item->phone_number}}</td>
                                <td>{{ $item->email}}</td>
                                <td>{{ $item->alamat}}</td>
                                <td>{{ $item->pendidikan}}</td>
                            </tr>
                            @endforeach
                            
                        </table>

                    </div>
                </div>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <footer class="main-footer">
            @include('Layout.footer')
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    @include('Layout.script')


</html>