<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Dashbord.Beranda');
})->name('start');

Route::get('/login', function () {
    return view('Awal.Login');
})->name('login');

Route::post('/postlogin', 'LoginController@postlogin')->name('postlogin');
Route::get('/logout', 'LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    route::get('/Beranda', 'BerandaController@index');
});

Route::get('/Guru-data', 'GuruController@index')->name('Guru-data');
Route::get('/Guru-create', 'GuruController@create')->name('Guru-create');
Route::post('/Guru-store', 'GuruController@store')->name('Guru-store');
Route::get('/Guru-show', 'GuruController@store')->name('Guru-show');#belum
Route::post('/Guru-edit', 'GuruController@store')->name('Guru-edit');#belum
Route::post('/Guru-update', 'GuruController@store')->name('Guru-update');#belum

Route::get('/Mapel-data', 'MapelController@index')->name('Mapel-data');
Route::get('/Mapel-create', 'MapelController@create')->name('Mapel-create');
Route::post('/Mapel-store', 'MapelController@store')->name('Mapel-store');

Route::get('/Siswa-data', 'SiswaController@index')->name('Siswa-data');
Route::get('/Siswa-create', 'SiswaController@create')->name('Siswa-create');
Route::post('/Siswa-store', 'SiswaController@store')->name('Siswa-store');

Route::get('/Kelas-data', 'KelasController@index')->name('Kelas-data');
Route::get('/Kelas-create', 'KelasController@create')->name('Kelas-create');
Route::post('/Kelas-store', 'KelasController@store')->name('Kelas-store');
Route::get('/Kelas-delete', 'KelasController@destroy')->name('Kelas-delete');

Route::get('/Jadwal-data', 'JadwalController@index')->name('Jadwal-data');
Route::get('/Jadwal-create', 'JadwalController@create')->name('Jadwal-create');
Route::post('/Jadwal-store', 'JadwalController@store')->name('Jadwal-store');

